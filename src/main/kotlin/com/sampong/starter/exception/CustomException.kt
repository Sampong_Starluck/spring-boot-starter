package com.sampong.starter.exception

import com.sampong.starter.model.enumerate.HttpCode

class CustomException(status: HttpCode, message: String) : RuntimeException(message) {
	var status: Int = status.value
	var error: String = status.reasonPhrase
}