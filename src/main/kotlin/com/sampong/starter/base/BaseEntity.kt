package com.sampong.starter.base

import com.fasterxml.jackson.annotation.JsonIgnore
import com.sampong.starter.model.master.User
import jakarta.persistence.*
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.util.*

@EntityListeners(AuditingEntityListener::class)
@MappedSuperclass
open class BaseEntity(
	@Column(name = "status", nullable = false)
	var status: Boolean = true,
	@Version
	@JsonIgnore
	@Column(name = "version")
	var version: Int? = 0,
	
	@CreatedDate
	var created: Date? = null,
	
	@LastModifiedDate
	var updated: Date? = null,
	
	@JsonIgnore
	@CreatedBy
	@OneToOne
	@JoinColumn(name = "created_by_id")
	var createdBy: User? = null,
	
	@JsonIgnore
	@LastModifiedBy
	@OneToOne
	@JoinColumn(name = "updated_by_id")
	var updatedBy: User? = null,
)