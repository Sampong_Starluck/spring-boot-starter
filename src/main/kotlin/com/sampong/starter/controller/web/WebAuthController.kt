package com.sampong.starter.controller.web

import com.sampong.starter.exception.CustomException
import com.sampong.starter.model.enumerate.HttpCode
import com.sampong.starter.model.request.JwtRequest
import com.sampong.starter.model.response.JwtResponse
import com.sampong.starter.model.request.SignupRequest
import com.sampong.starter.model.response.ResponseObjectMap
import com.sampong.starter.security.JwtToken
import com.sampong.starter.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.DisabledException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.web.bind.annotation.*

@RestController
//@CrossOrigin
@RequestMapping("/web/auth")
class WebAuthController {
	
	@Autowired
	lateinit var userService: UserService
	
	@Autowired
	lateinit var authenticationManager: AuthenticationManager
	
	@Autowired
	lateinit var jwtToken: JwtToken
	
	@Autowired
	lateinit var response: ResponseObjectMap
	
	@PostMapping("/login")
	fun createToken(@RequestBody t: JwtRequest): MutableMap<String, Any> {
		try {
			authenticationManager.authenticate(
				UsernamePasswordAuthenticationToken(
					t.name, t.pass
				)
			)
		} catch (e: DisabledException){
//			throw java.lang.Exception("Disabled_User", e)
			throw CustomException(HttpCode.NOT_ACCEPTABLE, "$e")
		} catch (e: BadCredentialsException){
//			throw Exception("Invalid_Credential", e)
			throw CustomException(HttpCode.INVALID_CREDENTIALS, "$e")
		}
		val usersDetail = userService.loadUserByUsername(t.name)
		val jwtTokens = jwtToken.generateJwtToken(usersDetail)
		
		return response.responseObject(JwtResponse(jwtTokens))
	}
	
	@PostMapping("/register")
	fun createUser(@RequestBody t: SignupRequest): MutableMap<String, Any>{
		val newUserDetail = userService.createNewUser(t)
		val jwtTokens = jwtToken.generateJwtToken(newUserDetail)
		return response.responseObject(JwtResponse(jwtTokens))
	}
}