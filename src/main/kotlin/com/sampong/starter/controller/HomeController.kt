package com.sampong.starter.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/home")
class HomeController {
	
	@GetMapping
	fun getHomeString(): String{
		return "Hello world to Spring boot 3 !!!!"
	}
}