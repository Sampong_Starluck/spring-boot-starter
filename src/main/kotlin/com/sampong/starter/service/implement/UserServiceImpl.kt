package com.sampong.starter.service.implement

import com.sampong.starter.exception.CustomException
import com.sampong.starter.model.enumerate.HttpCode
import com.sampong.starter.model.master.User
import com.sampong.starter.model.master.UsersDetails
import com.sampong.starter.model.request.SignupRequest
import com.sampong.starter.repository.master.UserDetailRepository
import com.sampong.starter.repository.master.UserRepository
import com.sampong.starter.service.UserService
import com.sampong.starter.utils.AESUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.security.core.userdetails.User as UserLog

@Service
class UserServiceImpl: UserService {
	@Autowired
	lateinit var userRepository: UserRepository
	@Autowired
	lateinit var userDetailRepository: UserDetailRepository
	
	@Value("\${aes.initial-vector}")
	var initVector: String= ""
	
	@Value("\${aes.secret-key}")
	var key: String = ""
	
	
	@Throws(UsernameNotFoundException::class)
	override fun loadUserByUsername(username: String): UserLog {
		val user = userRepository.findByUsernameAndStatusTrue(username)?: throw CustomException(
			HttpCode.NOT_FOUND, "User with the username $username does not existed !!!!"
		)
//			?: throw UsernameNotFoundException(username)
//		println(">>>>>>>>>>>>>>>>>>>>>>>>>>password")
//		println(AESUtils.decrypt(key, initVector,user.password))
//		println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
		return UserLog(user.username, AESUtils.decrypt(key, initVector,user.password), ArrayList())
	}
	
	override fun createNewUser(t: SignupRequest): UserLog {
		val user = userRepository.findByUsernameAndStatusTrue(t.username!!)
		if (user != null) throw Exception("User is already existed")
		
		/**
		 * Save user detail before save user and generate token
		 * */
		val newUser = User()
		newUser.email = t.email
		newUser.username = t.username
//		newUser.password = Argon2PasswordEncoder(5, 256, 1, 4096, 5)
//			.encode(t.password)
		val pass = BCryptPasswordEncoder().encode(t.password)
		newUser.password = AESUtils.encrypt(key, initVector, pass)
//		println(">>>>>>>>>>>>>>>>>>>>>>>>>encryption")
//		println(t.password)
//		println(newUser.password)
//		println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
		val users =userRepository.save(newUser)
		val usersDetails = UsersDetails(
			username = t.username,
			nationlId = t.nationalId,
			passport = t.passport,
			dob = t.dob,
			phoneNumber = t.phoneNumber,
			role = t.role,
			user = users
		)
		userDetailRepository.save(usersDetails)
//		newUser.usersDetails = detail
		
		
		val tokens = userRepository.findByUsernameAndStatusTrue(newUser.username!!)?: throw CustomException(
			HttpCode.NOT_FOUND, "User with the username ${newUser.username} does not existed !!!!"
		)
//			?: throw UsernameNotFoundException(newUser.username!!)

		
//		println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>decryption")
//		println(AESUtils.decrypt(key, initVector,tokens.password))
//		println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
		
		return UserLog(tokens.username, AESUtils.decrypt(key, initVector,tokens.password), ArrayList())
	}
}