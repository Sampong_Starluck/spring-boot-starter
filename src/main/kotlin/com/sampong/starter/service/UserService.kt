package com.sampong.starter.service

import com.sampong.starter.model.request.SignupRequest
import org.springframework.security.core.userdetails.User as UserLog
import org.springframework.security.core.userdetails.UserDetailsService

interface UserService: UserDetailsService {
	override fun loadUserByUsername(username: String): UserLog
	fun createNewUser(t: SignupRequest): UserLog
}