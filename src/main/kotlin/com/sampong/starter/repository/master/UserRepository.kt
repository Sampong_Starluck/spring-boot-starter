package com.sampong.starter.repository.master

import com.sampong.starter.base.BaseRepository
import com.sampong.starter.model.master.User
import org.springframework.stereotype.Repository

@Repository
interface UserRepository: BaseRepository<User> {
	fun findByEmailAndStatusTrue(user: String): User?
	fun findByUsernameAndStatusTrue(username: String): User?
}