package com.sampong.starter.repository.master

import com.sampong.starter.base.BaseRepository
import com.sampong.starter.model.master.UsersDetails
import org.springframework.stereotype.Repository

@Repository
interface UserDetailRepository: BaseRepository<UsersDetails> {
	fun findByUsernameAndStatusTrue(username: String?): UsersDetails?
}