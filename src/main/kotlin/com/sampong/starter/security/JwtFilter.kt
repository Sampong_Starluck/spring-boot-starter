package com.sampong.starter.security

import com.sampong.starter.service.UserService
import io.jsonwebtoken.ExpiredJwtException
import jakarta.servlet.FilterChain
import jakarta.servlet.ServletException
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException

@Component
class JwtFilter : OncePerRequestFilter() {
	@Autowired
	lateinit var userService: UserService
	
	@Autowired
	private val jwtToken: JwtToken? = null
	
	@Throws(ServletException::class, IOException::class)
	override fun doFilterInternal(
		request: HttpServletRequest,
		response: HttpServletResponse, filterChain: FilterChain
	) {
//		logger.info("Start =====>> Do internal filter")
		val tokenHeader = request.getHeader("Authorization")
		var username: String? = null
		var token: String? = null
		if (tokenHeader != null && (tokenHeader.startsWith("bearer ") || tokenHeader.startsWith("Bearer "))) {
			token = tokenHeader.substring(7)
			try {
				username = jwtToken?.getUsernameFromToken(token)
			} catch (e: IllegalArgumentException) {
//				println("Unable to get JWT Token")
				logger.info(e)
			} catch (e: ExpiredJwtException) {
//				println("JWT Token has expired")
				logger.info(e)
			}
		}
//		else {
//			println("Bearer String not found in token")
//			logger.info("Bearer String not found in token")
//		}
		if (null != username && SecurityContextHolder.getContext().authentication == null) {
			val userDetails: UserDetails = userService.loadUserByUsername(username)
			if (jwtToken?.validateJwtToken(token, userDetails) == true) {
				val authenticationToken = UsernamePasswordAuthenticationToken(
					userDetails, null,
					userDetails.authorities
				)
				authenticationToken.details = WebAuthenticationDetailsSource().buildDetails(request)
				SecurityContextHolder.getContext().authentication = authenticationToken
			}
		}
		filterChain.doFilter(request, response)
	}
}