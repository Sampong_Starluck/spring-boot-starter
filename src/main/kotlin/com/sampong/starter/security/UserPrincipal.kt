package com.sampong.starter.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails


class UserPrincipal(
	var name: String? = null,
	var pass: String? = null,
	var status: Boolean = true,
	var enable: Boolean = false,
	var auth: Collection<GrantedAuthority?>? = null
) : UserDetails {
	
	override fun getAuthorities(): Collection<GrantedAuthority?> {
		return auth!!
	}
	
	override fun getPassword(): String {
		return pass!!
	}
	
	override fun getUsername(): String {
		return name!!
	}
	
	override fun isAccountNonExpired(): Boolean {
		return true
	}
	
	override fun isAccountNonLocked(): Boolean {
		return true
	}
	
	override fun isCredentialsNonExpired(): Boolean {
		return true
	}
	
	override fun isEnabled(): Boolean {
		return enable
	}
	
	companion object {
		private const val serialVersionUID = 1L
	}
}