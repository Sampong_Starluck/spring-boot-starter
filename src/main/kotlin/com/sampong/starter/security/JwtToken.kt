package com.sampong.starter.security

import io.jsonwebtoken.Jws
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import java.util.*
import javax.crypto.spec.SecretKeySpec
import kotlin.collections.HashMap


@Component
class JwtToken: java.io.Serializable {
	private val serialVersionUID = 7008375124389347049L
	val TOKEN_VALIDITY = (10 * 60 * 60).toLong()
	@Value("\${jwt.secret}")
	private val jwtSecret: String? = null
	fun generateJwtToken(userDetails: UserDetails): String? {
		val claims: Map<String, Any> = HashMap()
		return Jwts.builder().setClaims(claims).setSubject(userDetails.username)
			.setIssuedAt(Date(System.currentTimeMillis()))
			.setExpiration(Date(System.currentTimeMillis() + TOKEN_VALIDITY * 1000))
			.signWith(SecretKeySpec(jwtSecret?.toByteArray(),"SHA512" ), SignatureAlgorithm.HS512).compact()
//			.signWith(SignatureAlgorithm.HS512, jwtSecret).compact()
	}
	
	fun validateJwtToken(token: String?, userDetails: UserDetails): Boolean? {
		val username = getUsernameFromToken(token)
		val claims = Jwts.parserBuilder().setSigningKey(jwtSecret?.toByteArray()).build().parseClaimsJws(token).body
		val isTokenExpired = claims.expiration.before(Date())
		return username == userDetails.username && !isTokenExpired
	}
	
	fun getUsernameFromToken(token: String?): String {
		val claims = Jwts.parserBuilder().setSigningKey(jwtSecret?.toByteArray()).build().parseClaimsJws(token).body
		return claims.subject
	}
}