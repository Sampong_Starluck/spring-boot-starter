package com.sampong.starter.config
//
//import org.apache.catalina.Context
//import org.apache.catalina.connector.Connector
//import org.apache.tomcat.util.descriptor.web.SecurityCollection
//import org.apache.tomcat.util.descriptor.web.SecurityConstraint
//import org.springframework.beans.factory.annotation.Value
//import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory
//import org.springframework.boot.web.servlet.server.ServletWebServerFactory
//import org.springframework.context.annotation.Bean
//import org.springframework.context.annotation.Configuration
//
//
//@Configuration
//class ServerConfig {
//	@Value("\${server.port}")
//	val server: Int?= null
//
//	@Bean
//	fun servletContainer(): ServletWebServerFactory {
//		val tomcat: TomcatServletWebServerFactory = object : TomcatServletWebServerFactory() {
//			override fun postProcessContext(context: Context) {
//				val securityConstraint = SecurityConstraint()
//				securityConstraint.userConstraint = "CONFIDENTIAL"
//				val collection = SecurityCollection()
//				collection.addPattern("/*")
//				securityConstraint.addCollection(collection)
//				context.addConstraint(securityConstraint)
//			}
//		}
//		tomcat.addAdditionalTomcatConnectors(httpConnector)
//		return tomcat
//	}
//
//	private val httpConnector: Connector
//		get() {
//			val connector = Connector(TomcatServletWebServerFactory.DEFAULT_PROTOCOL)
//			connector.scheme = "http"
//			connector.port = 8080
//			connector.secure = false
//			connector.redirectPort = server!!
//			return connector
//		}
//}