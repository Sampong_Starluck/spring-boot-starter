package com.sampong.starter.config
//
//import com.ulisesbocchio.jasyptspringboot.EncryptablePropertyResolver
//import org.jasypt.encryption.pbe.PooledPBEStringEncryptor
//import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig
//import org.springframework.beans.factory.annotation.Configurable
//import org.springframework.context.annotation.Bean
//import org.springframework.context.annotation.Configuration
//
//@Configuration
//internal class JasyptCustomEncryptor(password: CharArray?) :
//	EncryptablePropertyResolver {
//	private val encryptor: PooledPBEStringEncryptor = PooledPBEStringEncryptor()
//
//	init {
//		val config = SimpleStringPBEConfig()
//		config.passwordCharArray = password
//		config.algorithm = "PBEWITHHMACSHA512ANDAES_256"
//		config.setKeyObtentionIterations("1000")
//		config.poolSize = 1
//		config.providerName = "SunJCE"
//		config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator")
//		config.setIvGeneratorClassName("org.jasypt.iv.RandomIvGenerator")
//		config.stringOutputType = "base64"
//		encryptor.setConfig(config)
//	}
//	@Bean("jasyptStringEncryptor")
//	override fun resolvePropertyValue(value: String?): String {
//		return if (value != null && value.startsWith("{cipher}")) {
//			encryptor.decrypt(value.substring("{cipher}".length))
//		} else value!!
//	}
//}