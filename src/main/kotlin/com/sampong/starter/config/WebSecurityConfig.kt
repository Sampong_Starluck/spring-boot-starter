package com.sampong.starter.config

import com.sampong.starter.security.JwtAuthenticationEntryPoint
import com.sampong.starter.security.JwtFilter
import com.sampong.starter.service.UserService
import com.sampong.starter.utils.Hmac512PasswordEncoder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.DelegatingPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter
import java.util.*
import org.springframework.web.cors.CorsConfiguration as CorsConfig


@Configuration
@EnableWebSecurity
@EnableMethodSecurity(prePostEnabled = true)
class WebSecurityConfig {
	
	@Autowired
	lateinit var userDetailsService: UserService
	
	@Autowired
	lateinit var filter: JwtFilter
	
	@Autowired
	lateinit var jwtAuthenticationEntryPoint: JwtAuthenticationEntryPoint
	
//	@Bean
//	fun passwordEncoder(): PasswordEncoder {
////		return Argon2PasswordEncoder(5, 256, 1, 4096, 5)
//		return BCryptPasswordEncoder()
//	}
	
	/*
	 * creating custom password encoder using
	 * DelegatingPasswordEncoder and
	 * custom Hmac512PasswordEncoder
	 * @link: https://stackoverflow.com/questions/66437063/how-to-encode-password-with-hmac-sha512-in-spring-boot-security
	 */
	@Bean
	fun passwordEncoder(): PasswordEncoder? {
		val encoders: MutableMap<String, PasswordEncoder> = HashMap()
		encoders["SSHA-512"] = Hmac512PasswordEncoder("salt")
		encoders["bcrypt"] = BCryptPasswordEncoder()
		return DelegatingPasswordEncoder("SSHA-512", encoders)
	}
	
	@Bean
	@Throws(java.lang.Exception::class)
	fun authenticationManager(authenticationConfiguration: AuthenticationConfiguration): AuthenticationManager? {
		return authenticationConfiguration.authenticationManager
	}
	
//	@Bean
//	fun webSecurityCustomizer(): WebSecurityCustomizer? {
//		return WebSecurityCustomizer { web: WebSecurity -> web.ignoring().requestMatchers(
//			"/v3/api-docs",
//			"/configuration/ui",
//			"/swagger-resources/",
//			"/configuration/security",
//			"/swagger-ui",
//			"/webjars/",
//			"/resources/**"
//		)
//		}
//	}
	@Bean
	@Throws(Exception::class)
	fun configure(http: HttpSecurity): SecurityFilterChain? {
		
		// Configure AuthenticationManagerBuilder
		val authenticationManagerBuilder = http.getSharedObject(
			AuthenticationManagerBuilder::class.java
		)
		authenticationManagerBuilder.userDetailsService<UserDetailsService>(userDetailsService)
			.passwordEncoder(passwordEncoder())
		// Get AuthenticationManager
		val authenticationManager = authenticationManagerBuilder.build()
		http.csrf().disable().cors().and()
			// forward port from 8080 -> 8443 and change from http -> https
//			.requiresChannel { channel -> channel.anyRequest().requiresSecure() }
//			.authorizeHttpRequests { authorize -> authorize.anyRequest().permitAll() }
			.authorizeHttpRequests()
			.requestMatchers("/**").permitAll()
			.requestMatchers("/**/auth/login", "/**/auth/forgot-password", "/**/auth/reset-password", "/**/generate-unique-device").permitAll()
			.anyRequest()
			.authenticated().and()
			.exceptionHandling()
			.authenticationEntryPoint(jwtAuthenticationEntryPoint)
			.and()
			.authenticationManager(authenticationManager).sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		http.headers().frameOptions().disable()
		http.addFilterBefore(filter, UsernamePasswordAuthenticationFilter::class.java) // User Authorization with JWT before http
	return http.build()
	}
	
	@Bean
	fun corsFilter(): CorsFilter? {
		val corsConfiguration = CorsConfig()
		corsConfiguration.allowCredentials = true
		corsConfiguration.allowedOrigins = listOf("*")
		corsConfiguration.allowedHeaders = listOf(
			"Origin", "Access-Control-Allow-Origin",
			"Content-Type", "Accept", "Authorization", "Origin,Accept", "X-Requested-With",
			"Access-Control-Request-Method", "Access-Control-Request-Headers"
		)
		corsConfiguration.exposedHeaders = listOf(
			"Origin", "Content-Type", "Accept", "Authorization",
			"Access-Control-Allow-Origin", "Access-Control-Allow-Origin", "Access-Control-Allow-Credentials"
		)
		corsConfiguration.allowedMethods = listOf("GET", "PUT", "POST", "DELETE", "OPTIONS")
		val urlBasedCorsConfigurationSource = UrlBasedCorsConfigurationSource()
		urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration)
		return CorsFilter(urlBasedCorsConfigurationSource)
	}
}
