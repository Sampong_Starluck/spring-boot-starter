package com.sampong.starter.config

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.info.License
import io.swagger.v3.oas.annotations.servers.Server
import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.security.SecurityRequirement
import io.swagger.v3.oas.models.security.SecurityScheme
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@OpenAPIDefinition(
	info = Info(
		title = "My test API",
		version = "\${api.version}",
		contact = Contact(
			name = "Sampong",
			email = "sampong@admin.com",
			url = "https://www.google.com",
		),
		license = License(
			name = "Apache 2.0",
			url = "https://www.apache.org/licenses/LICENSE-2.0"
		),
//		termsOfService = "${tos.uri}",
//		description = "${api.description}"
	),
	servers = [Server(
		url = "\${api.server.url}",
		description = "\${spring.profiles.active}"
	)]
)
//@SecurityScheme(
//	name = "Bearer Authentication",
//	type = SecuritySchemeType.HTTP,
//	bearerFormat = "JWT",
//	scheme = "bearer"
//)
class OpenAPISecurityConfiguration {
	@Bean
	fun openAPIConfig(): OpenAPI {
		val securitySchemeName = "bearerAuth"
		
		return OpenAPI()
			.addSecurityItem(
				SecurityRequirement()
					.addList(securitySchemeName)
			)
			.components(
				Components()
					.addSecuritySchemes(
						securitySchemeName,
						SecurityScheme()
							.name(securitySchemeName)
							.type(SecurityScheme.Type.HTTP)
							.scheme("bearer")
							.bearerFormat("JWT")
					)
			)
	}
}