package com.sampong.starter.utils

import com.sampong.starter.exception.CustomException
import com.sampong.starter.model.enumerate.HttpCode
import jakarta.servlet.http.HttpServletRequest
import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.IOException
import java.net.MalformedURLException
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.text.SimpleDateFormat
import java.util.*

class FileUpload {
    /**
     * Can call class to using anywhere like singleton
     */
    companion object {

        fun storeImage(file: MultipartFile, filePath:String): String {
            val path = Paths.get(filePath).toAbsolutePath().normalize()
            val directory = File(path.toString())
            if (!directory.exists()) {
                directory.mkdirs()
            }

            val extension = file.originalFilename.toString()
            val sub = extension.substring(extension.lastIndexOf("."))
            val formatter = SimpleDateFormat("yyyy_MM_dd_HHmmss")
            val nameFile = UUID.randomUUID().toString().plus(formatter.format(Date()).toString()).plus(sub)

            try {
                Files.copy(file.inputStream, path.resolve(nameFile), StandardCopyOption.REPLACE_EXISTING)
            } catch (e: IOException) {
                return null.toString()
            }
            return nameFile
        }

        fun storeFile(file:MultipartFile, filePath: String): String {
            val path = Paths.get(filePath).toAbsolutePath().normalize()
            val directory = File(path.toString())
            if (!directory.exists()) {
                directory.mkdirs()
            }

            val originalFileName = "_"+file.originalFilename.toString()
            val formatter = SimpleDateFormat("yyyy_MM_dd_HHmmss")
            val nameFile = formatter.format(Date()).toString().plus(originalFileName)

            try {
                Files.copy(file.inputStream, path.resolve(nameFile), StandardCopyOption.REPLACE_EXISTING)
            } catch (e: IOException) {
                return null.toString()
            }
            return nameFile
        }

        fun storeImage(file:MultipartFile, filePath:String, fileName: String): String {
            val path = Paths.get(filePath).toAbsolutePath().normalize()
            val directory = File(path.toString())
            if (!directory.exists()) {
                directory.mkdirs()
            }

            try {
                Files.copy(file.inputStream, path.resolve(fileName), StandardCopyOption.REPLACE_EXISTING)
            } catch (e: IOException) {
                return null.toString()
            }
            return fileName
        }

        fun removeImage (fileName : String,fileProperty:String): Boolean {
            val path = Paths.get(fileProperty).toAbsolutePath().normalize()
            val filePath = path.resolve(fileName).normalize()
            val file = File(filePath.toString())

            return if(file.exists()) file.delete()
            else false
        }

        fun loadFile(fileName:String, fileProperty:String, request: HttpServletRequest, download: Boolean) : ResponseEntity<Resource> {
            try {
                val path = Paths.get(fileProperty).toAbsolutePath().normalize()
                val filePath = path.resolve(fileName).normalize()
                val resource = UrlResource(filePath.toUri())
                resource.contentLength()

                if (download) {
                    val contentType = request.servletContext.getMimeType(resource.file.absolutePath)
                            ?: "application/octet-stream"
                    return ResponseEntity.ok()
                            .contentType(MediaType.parseMediaType(contentType))
                            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.filename + "\"")
                            .body(resource)
                }

                val contentType = request.servletContext.getMimeType(resource.file.absolutePath)
                        ?: throw CustomException(HttpCode.NOT_ACCEPTABLE, "Invalid file type.")

                return ResponseEntity.ok()
                        .contentType(MediaType.parseMediaType(contentType))
                        .body(resource)
            } catch (e: MalformedURLException) {
                throw CustomException(HttpCode.BAD_REQUEST, e.message!!)
            } catch (e: IOException) {
                throw CustomException(HttpCode.NOT_FOUND, "File not found.")
            }
        }
     }
 }