package com.sampong.starter.utils

import com.sampong.starter.exception.CustomException
import com.sampong.starter.model.enumerate.HttpCode
import java.io.UnsupportedEncodingException
import java.nio.charset.StandardCharsets
import java.security.InvalidAlgorithmParameterException
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.util.*
import javax.crypto.BadPaddingException
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException
import javax.crypto.NoSuchPaddingException
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

object AESUtils {
	fun encrypt(key: String, initVector: String, value: String): String {
		try {
			val iv = IvParameterSpec(initVector.toByteArray(charset("UTF-8")))
			val skeySpec = SecretKeySpec(key.toByteArray(charset("UTF-8")), "AES")
			
			val cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING")
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv)
			
			val encrypted = cipher.doFinal(value.toByteArray())
			
			return Base64.getEncoder().encodeToString(encrypted)
		} catch (ex: UnsupportedEncodingException) {
			throw RuntimeException("Encrypt exception: ", ex)
		} catch (ex: InvalidAlgorithmParameterException) {
			throw RuntimeException("Encrypt exception: ", ex)
		} catch (ex: InvalidKeyException) {
			throw RuntimeException("Encrypt exception: ", ex)
		} catch (ex: NoSuchAlgorithmException) {
			throw RuntimeException("Encrypt exception: ", ex)
		} catch (ex: BadPaddingException) {
			throw RuntimeException("Encrypt exception: ", ex)
		} catch (ex: IllegalBlockSizeException) {
			throw RuntimeException("Encrypt exception: ", ex)
		} catch (ex: NoSuchPaddingException) {
			throw RuntimeException("Encrypt exception: ", ex)
		}
	}
	
	fun decrypt(key: String, initVector: String, encrypted: String?): String {
		return try {
			val iv = IvParameterSpec(initVector.toByteArray(StandardCharsets.UTF_8))
			val skeySpec = SecretKeySpec(key.toByteArray(StandardCharsets.UTF_8), "AES")
			val cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING")
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv)
			val original = cipher.doFinal(Base64.getDecoder().decode(encrypted))
			String(original)
		} catch (ex: IllegalArgumentException) {
			throw CustomException(HttpCode.NOT_ACCEPTABLE, "Invalid data provided!")
		} catch (ex: InvalidAlgorithmParameterException) {
			throw CustomException(HttpCode.NOT_ACCEPTABLE, "Invalid data provided!")
		} catch (ex: InvalidKeyException) {
			throw CustomException(HttpCode.NOT_ACCEPTABLE, "Invalid data provided!")
		} catch (ex: NoSuchAlgorithmException) {
			throw CustomException(HttpCode.NOT_ACCEPTABLE, "Invalid data provided!")
		} catch (ex: BadPaddingException) {
			throw CustomException(HttpCode.NOT_ACCEPTABLE, "Invalid data provided!")
		} catch (ex: IllegalBlockSizeException) {
			throw CustomException(HttpCode.NOT_ACCEPTABLE, "Invalid data provided!")
		} catch (ex: NoSuchPaddingException) {
			throw CustomException(HttpCode.NOT_ACCEPTABLE, "Invalid data provided!")
		}
	}
}