package com.sampong.starter.utils

import org.springframework.security.crypto.codec.Utf8
import org.springframework.security.crypto.password.PasswordEncoder
import java.security.InvalidKeyException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

/*
 *
 * Creating a password encoder using Hmac512 algorithm inspire by
 * deprecated function LdapSha512PasswordEncoder.
 * @url:  https://github.com/lathspell/java_test/blob/master/java_test_openldap/src/main/java/LdapSha512PasswordEncoder.java
 *
 */

class Hmac512PasswordEncoder(salt: String?) : PasswordEncoder {
	private val salt: String
	
	init {
		requireNotNull(salt) { "salt cannot be null" }
		this.salt = salt
	}
	
	override fun encode(rawPassword: CharSequence): String {
		var result: String? = null
		try {
			val sha512Hmac: Mac = Mac.getInstance(HMAC_SHA512)
			val byteKey = Utf8.encode(salt)
			val keySpec = SecretKeySpec(byteKey, HMAC_SHA512)
			sha512Hmac.init(keySpec)
			val macData: ByteArray = sha512Hmac.doFinal(Utf8.encode(rawPassword.toString()))
			result = SSHA512_PREFIX + Base64.getEncoder().encodeToString(macData)
			//result = bytesToHex(macData);
		} catch (e: InvalidKeyException) {
			e.printStackTrace()
		} catch (e: NoSuchAlgorithmException) {
			e.printStackTrace()
		}
		return result!!
	}
	
	override fun matches(rawPassword: CharSequence? , encodedPassword: String? ): Boolean {
		if (rawPassword == null || encodedPassword == null) {
			return false
		}
		val encodedRawPass = encode(rawPassword)
		return MessageDigest.isEqual(Utf8.encode(encodedRawPass), Utf8.encode(encodedPassword))
	}
	
	companion object {
		private const val SSHA512_PREFIX = "{SSHA-512}"
		private const val HMAC_SHA512 = "HmacSHA512"
	}
}