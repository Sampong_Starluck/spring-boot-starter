package com.sampong.starter.model.master

import com.fasterxml.jackson.annotation.JsonIgnore
import com.sampong.starter.base.BaseEntity
import com.sampong.starter.model.enumerate.UserRole
import jakarta.persistence.*

@Entity
@Table(name = "mas_users_details")
class UsersDetails(
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	var id: Long?= null,
	@Column(name = "username")
	var username: String?= null,
	@Column(name = "national_id")
	var nationlId: String?= null,
	@Column(name = "passport")
	var passport: String?= null,
	@Column(name = "date_of_birth")
	var dob: String?= null,
	@Column(name = "phone_number")
	var phoneNumber: String?= null,
	@Enumerated(EnumType.STRING)
	var role: UserRole?= UserRole.USER,
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	var user: User?= null
): BaseEntity()