package com.sampong.starter.model.master

import com.sampong.starter.base.BaseEntity
import jakarta.persistence.*

@Entity
@Table(name = "mas_users")
class User (
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	var id: Long?= null,
	@Column(name = "email")
	var email: String?= null,
	@Column(name = "password")
	var password: String?= null,
	@Column(name = "username")
	var username: String?= null,
	@OneToOne(mappedBy = "user", fetch = FetchType.LAZY)
	var usersDetails: UsersDetails?= null
): BaseEntity()