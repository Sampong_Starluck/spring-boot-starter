package com.sampong.starter.model.enumerate

enum class HttpCode(val value: Int, val reasonPhrase: String) {
	BAD_REQUEST(400, "Bad Request"),
	UNAUTHORIZED(401, "Unauthorized"),
	INVALID_CREDENTIALS(401, "Invalid credentials"),
	FORBIDDEN(403, "Forbidden"),
	NOT_FOUND(404, "Not Found"),
	METHOD_NOT_ALLOWED(405, "Method Not Allowed"),
	NOT_ACCEPTABLE(406, "Not Acceptable"),
	GONE(410, "Gone"),
	VALIDATION(402, "Validation"),
	INTERNAL_SERVER_ERROR(500, "Internal Server Error"),
	SUCCESS(200, "Success")
}