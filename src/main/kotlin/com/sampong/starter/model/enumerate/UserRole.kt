package com.sampong.starter.model.enumerate

enum class UserRole {
	ADMIN,
	MANAGER,
	USER
}