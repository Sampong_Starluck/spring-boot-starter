package com.sampong.starter.model.request

data class ObjectIdRequest(
    var id: Long = 0
)