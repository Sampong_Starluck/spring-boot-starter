package com.sampong.starter.model.request

import com.sampong.starter.model.enumerate.UserRole

class SignupRequest {
	var email: String?= null
	var username: String?= null
	var password: String?= null
	var nationalId: String?= null
	var passport: String?= null
	var dob: String?= null
	var phoneNumber: String?= null
	var role: UserRole?= UserRole.USER
}