package com.sampong.starter.model.request

class JwtRequest(
	val serialVersionUID: Long =  2636936156391265891L,
	var name: String,
	var pass: String
): java.io.Serializable